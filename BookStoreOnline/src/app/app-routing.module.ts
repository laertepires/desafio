import { NgModule } from '@angular/core';
import { RouterModule, Route } from '@angular/router';
import { PagesComponent } from './pages/pages.component';
import { BooksComponent } from './pages/books/books.component';
import { BookDetailsComponent } from './pages/book-details/book-details.component';
import { BookDetailsResolver } from './pages/book-details/book.resolver';


const routes: Route[] = [
  {
    path: '',
    component: PagesComponent,
    children: [
      {
        path: '',
        redirectTo: 'books',
        pathMatch: 'full'
      },
      {
        path: 'books',
        component: BooksComponent
      },
      {
        path: 'books/:id',
        component: BookDetailsComponent,
        resolve: { book: BookDetailsResolver },
      },
    ]
  },
  {
    path: '**',
    redirectTo: 'login'
  }
];

@NgModule({
  imports: [
    RouterModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  providers: [BookDetailsResolver]
})
export class AppRoutingModule { }
