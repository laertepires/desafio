import { registerLocaleData } from '@angular/common';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  api = environment.apiUrl;

  constructor(private http: HttpClient) {}

  // login(params) {
  //   return this.http.post(this.api + '/signin', {
  //     email: params.email,
  //     password: params.password
  //   }).pipe(map((token: any) => {
  //     localStorage.setItem('token', JSON.stringify(token.accessToken));
  //   }));
  // }

  // logout() {
  //   localStorage.clear();
  //   window.location.reload();
  // }

}
