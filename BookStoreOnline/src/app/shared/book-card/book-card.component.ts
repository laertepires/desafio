import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BookType } from 'src/app/core/types/book.types';
import { BooksService } from 'src/app/core/services/books.services';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';

@Component({
  selector: 'app-book-card',
  templateUrl: './book-card.component.html',
  styleUrls: ['./book-card.component.scss']
})

export class BookCardComponent implements OnInit {

  @Input() book: BookType;
  @Output() rentEmit = new EventEmitter();
  @Input() favorite = false;

  constructor() { }

  ngOnInit(): void {
  }

  rent() {
    // if (!this.book.available) {
    //   this.rentEmit.emit(this.book);
    // }
  }

}
