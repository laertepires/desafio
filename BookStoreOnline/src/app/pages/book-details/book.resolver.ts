import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { BooksService } from 'src/app/core/services/books.services';


@Injectable()

export class BookDetailsResolver implements Resolve<any> {

  constructor(private service: BooksService) {}

  resolve(route: ActivatedRouteSnapshot) {
    const id = route.params.id;
    return this.service.getBookById(id);
  }
}
