import { Component, OnInit } from '@angular/core';
import { BooksService } from 'src/app/core/services/books.services';
import { BookType } from 'src/app/core/types/book.types';
import { ActivatedRoute } from '@angular/router';
import {MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition} from '@angular/material/snack-bar';

@Component({
  selector: 'app-book-details',
  templateUrl: './book-details.component.html',
  styleUrls: ['./book-details.component.scss']
})
export class BookDetailsComponent implements OnInit {

  book: BookType;
  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  constructor(
    private service: BooksService,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.book = this.route.snapshot.data.book;

    console.log('this.book -> ', this.book);
  }

  rent() {
    const payload: any = this.book;
    payload.available = true;

    // if (!this.book.available) {
    //   this.service.updateBook(this.book.id, payload).subscribe(() => {
    //     this.snackBar.open('Livro alugado com Sucesso', 'Undo', {
    //       duration: 3000,
    //       horizontalPosition: this.horizontalPosition,
    //       verticalPosition: this.verticalPosition,
    //     });
    //   });
    // }
  }
}
