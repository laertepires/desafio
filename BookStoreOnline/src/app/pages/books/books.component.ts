import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { BooksService } from 'src/app/core/services/books.services';
import {  BookType, BooksType } from 'src/app/core/types/book.types';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition, MatSnackBar } from '@angular/material/snack-bar';
import { MatPaginator } from '@angular/material/paginator';
import { SocialAuthService, GoogleLoginProvider, SocialUser } from 'angularx-social-login';



@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BooksComponent implements OnInit {

  /**
   * array books
   */
  books: BooksType;

  /**
   * form
   */
  form: FormGroup;

  user: SocialUser;
  loggedIn: boolean;

  constructor(
    public service: BooksService,
    private formBuilder: FormBuilder,
    private authService: SocialAuthService,
    private bookService: BooksService
  ) { }

  /**
   * init from component
   */
  ngOnInit(): void {

    this.form = this.formBuilder.group({
      search: ['']
    });
  }

  /**
   * submit search
   */
  onSubmit() {
    this.service.searchBookByTitle(this.form.get('search').value, 0).subscribe((data: any) => {
      this.books = data;
    });
  }

  favorite() {
    this.service.addBookToFavorite().subscribe(data => {
      console.log('data -> ', data);
    }, error => {
      console.log('error -> ', error);
    });
  }

  goToPage(event) {
    this.service.searchBookByTitle(this.form.get('search').value, event.pageIndex).subscribe((data: any) => {
      this.books = data;
    });
  }

  signInWithGoogle(): void {

    const googleLoginOptions: any = {
      scope: 'openid profile email'
    };

    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID, googleLoginOptions).then((response: any) => {
      sessionStorage.Authorization = 'Baerer ' + response.idToken;
    });
  }

  signOut(): void {
    this.authService.signOut();
  }
}
