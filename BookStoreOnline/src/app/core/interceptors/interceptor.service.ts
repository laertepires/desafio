import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class Interceptor implements HttpInterceptor {
 intercept( request: HttpRequest<any>, next: HttpHandler ): Observable<HttpEvent<any>> {
  console.log('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa -> ', sessionStorage.length );
  const dupReq = request.clone({
    headers: request.headers.set('Authorization', sessionStorage.length > 0 ?  sessionStorage['Authorization'] : ''),
  });
  return next.handle(dupReq);
 }
}
// this.defaultHeaders = this.defaultHeaders.append('Content-Type', 'application/json');
