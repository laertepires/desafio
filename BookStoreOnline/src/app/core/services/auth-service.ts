import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})

export class AuthService {

  saveToken(token) {
    // sessionStorage.Authorization = 'Baerer ' + token;
    localStorage.setItem("Authorization", token);
  }

}
