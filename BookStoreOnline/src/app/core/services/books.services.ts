import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { BookType, BooksType } from '../types/book.types';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})

export class BooksService {

  api = environment.apiUrl;

  constructor(
    private http: HttpClient
  ) { }


  getAllBooks() {
    return this.http.get(`${this.api}/books`);
  }

  getBookById(id) {
    return this.http.get(`${this.api}volumes/${id}`).pipe(map((data: any) => {

      const book = new BookType();
      book.author = data.volumeInfo.authors;
      book.description = data.volumeInfo.description;
      book.id = data.id;
      book.image = data.volumeInfo.imageLinks?.small;
      book.title = data.volumeInfo.title;

      return book;
    }));
  }

  addBookToFavorite(): Observable<any> {


    const params = {
      volumeId: 'JEbPokrIDbsC',

    };

    // tslint:disable-next-line: max-line-length
    return this.http.post(`${this.api}mylibrary/bookshelves/${0}/addVolume?volumeId=JEbPokrIDbsC&key=AIzaSyACYXL69H6RU-qhHSNAxaYIik10fSgsqhA`, params ).pipe(map((data: any) => {
      console.log(data);
      return data;
    }, error => {
      console.log(error);
    }));
  }

  searchBookByTitle(title, page: number = 0): Observable<BooksType> {
    return this.http.get(`${this.api}volumes?q=${title}&startIndex=${page}`).pipe(map((data: any) => {

      const books = new BooksType();
      books.total = data.totalItems;

      data.items.forEach(res => {
        const book = new BookType();
        book.author = res.volumeInfo.authors;
        book.description = res.volumeInfo.description;
        book.id = res.id;
        book.image = res.volumeInfo.imageLinks?.thumbnail;
        book.title = res.volumeInfo.title;

        books.books.push(book);
      });

      return books;
    }));
  }

  authBooks(): Observable<any> {
    return this.http.get(`${this.api}auth/books`).pipe(map(data => {
        return data;
      })
    );
  }

}

