export class BookType {
  id: string;
  title: string;
  author: string[] = [];
  image: string;
  description: string;
}

export class BooksType {
  total: number;
  books: BookType[] = [];
}
